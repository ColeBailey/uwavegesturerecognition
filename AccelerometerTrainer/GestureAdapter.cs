﻿using Android.Widget;
using uWaveAlgorithm;
using Android.Content;
using Android.Views;
using Android.App;
using System.Collections.Generic;

namespace AccelerometerTrainer
{
	public class GestureAdapter : ArrayAdapter<GestureRecognizedEventArgs>
	{
		Context mContext;
		int layoutResourceId;
		IList<GestureRecognizedEventArgs> elements = new List<GestureRecognizedEventArgs>();
		Facade facade;

		public GestureAdapter (Facade agent, Context c, int resourceId) : base(c, resourceId)
		{
			mContext = c;
			layoutResourceId = resourceId;
			facade = agent;
		}

		public void AddElement(GestureRecognizedEventArgs e) {
			Insert (e, 0);
			elements.Insert (0, e);
			NotifyDataSetChanged ();
		}

		public override Android.Views.View GetView (int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			
			LayoutInflater inflater = (mContext as Activity).LayoutInflater;
			convertView = inflater.Inflate (layoutResourceId, parent, false);



			var item = elements [position];

			TextView Id = convertView.FindViewById<TextView> (Resource.Id.title);
			Id.Text = item.GestureId;

			TextView Data = convertView.FindViewById<TextView> (Resource.Id.info);
			Data.Text = item.Cost.ToString();

			Button Approve = convertView.FindViewById<Button> (Resource.Id.approve);
			Approve.Click += (sender, e) => {
				facade.PositiveUpdate (item);
			};

			Button Deny = convertView.FindViewById<Button> (Resource.Id.reject);
			Deny.Click += (sender, e) => {
				EditText edit = new EditText (mContext);
				new AlertDialog.Builder (mContext)
				.SetTitle ("Input Actual Gesture Id")
				.SetView (edit)
				.SetPositiveButton ("OK", (thing, args) => {
					facade.NegativeUpdate (item, edit.Text);
				})
				.Show ();
			};


			return convertView;
		}
	}
}

