﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using uWaveAlgorithm;
using Android.Hardware;
using uWaveAlgorithm.DataInterpretation;

namespace AccelerometerTrainer
{
	[Activity (Label = "AccelerometerTrainer", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity, ISensorEventListener
	{
		Facade gestureRecognizer;
		SensorManager _sensorManager;
		ListView _list;
		GestureAdapter _adapter;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			gestureRecognizer = new Facade ();

			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button> (Resource.Id.myButton);
			
			button.Click += delegate {
				EditText edit = new EditText(this);
				new AlertDialog.Builder(this)
					.SetTitle("Input Actual Gesture Id")
					.SetView(edit)
					.SetPositiveButton("OK", (thing, args) => {
						gestureRecognizer.NextTrainingDataId = edit.Text;
					})
					.Show();
			};

			Button import = FindViewById<Button> (Resource.Id.ImportButton);
			import.Click += delegate {
				EditText edit = new EditText (this);
				new AlertDialog.Builder (this)
					.SetTitle ("Input Exported Gestures")
					.SetView (edit)
					.SetPositiveButton ("Import", (thing, args) => {
						gestureRecognizer.ImportGestureLibrary(edit.Text);
					})
					.Show ();
			}; 

			Button export = FindViewById<Button> (Resource.Id.ExportButton);
			export.Click += delegate {
				Intent sendIntent = new Intent();
				sendIntent.SetAction(Intent.ActionSend);
				sendIntent.PutExtra(Intent.ExtraText, gestureRecognizer.ExportGestureLibrary());
				sendIntent.SetType("text/plain");
				StartActivity(sendIntent);
			};
				
			_adapter = new GestureAdapter (gestureRecognizer, this, Resource.Layout.GestureItem);
			_list = FindViewById<ListView> (Resource.Id.list);
			_list.Adapter = _adapter;


			gestureRecognizer.GestureRecognized += (sender, e) => {
				RunOnUiThread(() => {
					_adapter.AddElement(e);
				});
			};

			_sensorManager = GetSystemService (Context.SensorService) as SensorManager;
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			_sensorManager.RegisterListener (this, _sensorManager.GetDefaultSensor(SensorType.Accelerometer), SensorDelay.Game);
			_sensorManager.RegisterListener (this, _sensorManager.GetDefaultSensor (SensorType.Gravity), SensorDelay.Game);
		}

		protected override void OnPause ()
		{
			base.OnPause ();
			_sensorManager.UnregisterListener (this);
		}

		public void OnAccuracyChanged (Sensor sensor, SensorStatus accuracy)
		{
			//
		}

		private float[] grav = new float[3];
		public void OnSensorChanged (SensorEvent e)
		{
			if (e.Sensor.Type == SensorType.Accelerometer) {
				gestureRecognizer.AccelerometerInput (e.Values [0] - grav[0], e.Values [1] - grav[1], e.Values [2] - grav[2]);

			} else if (e.Sensor.Type == SensorType.Gravity) {
				grav = new [] { e.Values [0], e.Values [1], e.Values [2] };
			}
		}
	}
}


