﻿using System;

namespace uWaveAlgorithm.DataInterpretation
{
	internal class Quantizor
	{
		internal double AverageRawValue { get; private set; }
		int valueSetSize;

		internal Quantizor ()
		{
		}

		internal void AddValue(double val) {
			double weightedValueSet = AverageRawValue * valueSetSize;
			valueSetSize++;
			AverageRawValue = (weightedValueSet + val) / valueSetSize;
		}

		internal int RetrieveQuantizedValue() {
			const double gravity = 9.8;
			const double doubleGravity = 2 * gravity;
			if (AverageRawValue < -1 * doubleGravity) {		// below -2g
				return -16;
			} else if (AverageRawValue < -1 * gravity) {		// between -2g and -g
				return -10 - NormalizedStepLevel (-1 * AverageRawValue, 5, gravity, doubleGravity);
			} else if (AverageRawValue < 0) {					// between -g and 0
				return -1 * NormalizedStepLevel (-1 * AverageRawValue, 10, 0, gravity);
			} else if (AverageRawValue < double.Epsilon) {	// between 0 and eps
				return 0;
			} else if (AverageRawValue < gravity) {			// between eps and 2g
				return NormalizedStepLevel (AverageRawValue, 10, 0, gravity);
			} else if (AverageRawValue < doubleGravity) {		// between g and 2g
				return 10 + NormalizedStepLevel (AverageRawValue, 5, gravity, doubleGravity);
			} else {										// over 2g
				return 16;
			}
		}

		private int NormalizedStepLevel(double value, int levelCount, double lowerBound, double higherBound) {
			if (value < lowerBound || value > higherBound) {
				throw new InvalidOperationException ("Value is not within given bounds");
			}
			double span = higherBound - lowerBound;
			double stepSpan = span / levelCount;
			double normalizedValue = value - lowerBound;

			for (int i = 1; i < levelCount + 1; i++) {
				if (normalizedValue < i * stepSpan) {
					return i;
				}
			}

			throw new InvalidOperationException ("Value is not within given bounds");
		}
	}
}

