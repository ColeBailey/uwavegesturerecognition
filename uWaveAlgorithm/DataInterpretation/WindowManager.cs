﻿using System;

namespace uWaveAlgorithm.DataInterpretation
{
	internal class WindowManager
	{
		internal Quantizor X { get; private set; }

		internal Quantizor Y { get; private set; }

		internal Quantizor Z { get; private set; }

		internal DateTime WindowElapsed { get; set; }

		internal WindowManager ()
		{
			X = new Quantizor ();
			Y = new Quantizor ();
			Z = new Quantizor ();
		}
	}
}

