﻿using System;
using System.Collections.Generic;

namespace uWaveAlgorithm.DataInterpretation
{
	internal class DataManager
	{
		internal EventHandler<IList<int[]>> DTWValueUpdate;

		internal List<int[]> CurrentDTW { get; private set; }

		const int EQUILIBRIUM_STEP_COUNT = 3;
		int equilibriumCounter = 0;
		readonly double equilibriumVariance;
		const double EQUILIBIRUM_FORCE = 0;
		readonly int WINDOW_SIZE_MILLISECONDS = 50;
		readonly int WINDOW_STEP_MILLISECONDS = 30;
		DateTime previousWindowCreatedStamp;

		List<WindowManager> windows = new List<WindowManager> ();

		internal DataManager (double allowedStabileVariance = 1.5)
		{
			equilibriumVariance = allowedStabileVariance;
			CurrentDTW = new List<int[]> ();

		}

		internal void FeedAccelerometerVector(double x, double y, double z) {
			var timestamp = DateTime.UtcNow;

			var closedWindows = new List<WindowManager> ();
			foreach (var w in windows) {
				if (timestamp > w.WindowElapsed) {			// window closed
					double averageRawForce = Math.Sqrt (Math.Pow (w.X.AverageRawValue, 2) + Math.Pow (w.Y.AverageRawValue, 2) + Math.Pow (w.Z.AverageRawValue, 2));
					bool isAtRest = Math.Abs (EQUILIBIRUM_FORCE - averageRawForce) < equilibriumVariance;

					UpdateDTW (w.X.RetrieveQuantizedValue (),
						w.Y.RetrieveQuantizedValue (),
						w.Z.RetrieveQuantizedValue (), 
						isAtRest);
					closedWindows.Add(w);
				} else {
					w.X.AddValue (x);
					w.Y.AddValue (y);
					w.Z.AddValue (z);
				}
			}
			foreach (WindowManager w in closedWindows) {
				windows.Remove (w);
			}

			// check if next window has started
			if (previousWindowCreatedStamp == null || timestamp.AddMilliseconds(WINDOW_STEP_MILLISECONDS) > previousWindowCreatedStamp) {
				var wm = new WindowManager {
					WindowElapsed = timestamp.AddMilliseconds(WINDOW_SIZE_MILLISECONDS)
				};
				wm.X.AddValue (x);
				wm.Y.AddValue (y);
				wm.Z.AddValue (z);

				windows.Add (wm);
				previousWindowCreatedStamp = timestamp;
			}
		}

		private void UpdateDTW(int x, int y, int z, bool isAtRest) {
			if (isAtRest) {		// phone is at rest
				if (CurrentDTW.Count == 0) {
					return;		// no motion detected yet, dispose of values
				} else if (equilibriumCounter < EQUILIBRIUM_STEP_COUNT) {		// add motionless data
					CurrentDTW.Add(new int[] { x, y, z });
					equilibriumCounter++;
				} else {		// gesture recognized, remove motionless data
					CurrentDTW.RemoveRange(CurrentDTW.Count - equilibriumCounter, equilibriumCounter);
					equilibriumCounter = 0;

					if (DTWValueUpdate != null) {
						DTWValueUpdate (this, new List<int[]> (CurrentDTW));
					}

					CurrentDTW.Clear ();
				}

			} else {			// phone is in motion
				CurrentDTW.Add(new int[] { x, y, z });
				equilibriumCounter = 0;
			}
		}
			
	}
}

