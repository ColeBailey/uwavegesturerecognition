﻿using System;
using System.Collections.Generic;

namespace uWaveAlgorithm.GestureLibrary
{
	internal class GestureCollection
	{
		internal IList<GestureTemplate> Templates { get; set; }

		internal GestureCollection ()
		{
			Templates = new List<GestureTemplate> ();
		}

		internal GestureCollection(int[] initialTemplateValues) {
			Templates = new List<GestureTemplate> ();
			GestureTemplate initialValues = new GestureTemplate () { Template = new List<int[]> () { initialTemplateValues }};
			Templates.Add (initialValues);
		}

		internal GestureTemplate AverageTemplates(GestureTemplate source, GestureTemplate target) {
			throw new NotImplementedException ();
		}

		internal void Update(IList<int[]> template) {
			var old = FindOldestTemplate ();
			Templates.Remove (old);



			Templates.Add(new GestureTemplate { Template = new List<int[]> (template) });
		}

		internal void RemoveDataset(IList<int[]> data) {
			foreach (GestureTemplate tem in Templates) {
				if (tem.Template == data) {
					Templates.Remove (tem);
				}
			}
		}

		internal double CalculateMatchingCost(IList<int[]> data) {
			double cost = double.PositiveInfinity;
			foreach (var template in Templates) {
				var localCost = template.DTWDistance (data);
				if (localCost < cost) {
					cost = localCost;
				}
			}

			return cost;
		}

		private GestureTemplate FindOldestTemplate() {
			if (Templates.Count == 0) {
				throw new InvalidOperationException ();
			}
			GestureTemplate oldest = Templates [0];
			foreach (GestureTemplate g in Templates) {
				if (g.TimeStamp < oldest.TimeStamp) {
					oldest = g;
				}
			}

			return oldest;
		}
	}
}

