﻿using System.Collections.Generic;

namespace uWaveAlgorithm.GestureLibrary
{
	internal class GestureManager
	{
		internal readonly Dictionary<string, GestureCollection> Library;
		readonly IReadOnlyList<string> IGNORED_GESTURE_IDS = new List<string> { "NeutralXPos", "NeutralXNeg", "NeutralYPos", "NeutralYNeg", "NeutralZPos", "NeutralZNeg", "Motionless" };

		internal GestureManager ()
		{
			Library = new Dictionary<string, GestureCollection> ();
			Library.Add (IGNORED_GESTURE_IDS [6], new GestureCollection (new int[] { 0, 0, 0 }));
//			Library.Add (IGNORED_GESTURE_IDS[0], new GestureCollection(new int[] { 11, 0, 0 }));
//			Library.Add (IGNORED_GESTURE_IDS[1], new GestureCollection(new int[] { -11, 0, 0 }));
//			Library.Add (IGNORED_GESTURE_IDS[2], new GestureCollection(new int[] { 0, 11, 0 }));
//			Library.Add (IGNORED_GESTURE_IDS[3], new GestureCollection(new int[] { 0, -11, 0 }));
//			Library.Add (IGNORED_GESTURE_IDS[4], new GestureCollection(new int[] { 0, 0, 11 }));
//			Library.Add (IGNORED_GESTURE_IDS[5], new GestureCollection(new int[] { 0, 0, -11 }));

		}

		internal void TrainNewGesture(string gestureId, IList<int[]> data) {
			var template = new GestureTemplate { Template = new List<int[]> (data) };
			if (Library.ContainsKey (gestureId)) {
				Library [gestureId].Templates.Add (template);
			} else {
				var collection = new GestureCollection ();
				collection.Templates.Add (template);
				Library.Add (gestureId, collection);
			}
		}

		internal void RetrainGesture(string gestureId, IList<int[]> data) {
			if (Library.ContainsKey (gestureId)) {
				Library [gestureId].Update (data);
			} else {
				TrainNewGesture (gestureId, data);
			}
		}

		internal void RemoveDataset(string gestureId, IList<int[]> data) {
			if (Library.ContainsKey (gestureId)) {
				Library [gestureId].RemoveDataset (data);
			}
		}

		internal void EraseGesture(string gestureId) {
			if (Library.ContainsKey (gestureId)) {
				Library.Remove (gestureId);
			}
		}

		internal string RecognizeGesture(IList<int[]> data) {
			double value;
			return RecognizeGesture (data, out value);
		}

		internal string RecognizeGesture(IList<int[]> data, out double cost) {
			string gesture = string.Empty;
			cost = double.PositiveInfinity;
			foreach (var gc in Library) {
				double localCost = gc.Value.CalculateMatchingCost (data);
				if (localCost < cost) {
					cost = localCost;
					gesture = gc.Key;
				}
			}

			return gesture;
		}

		internal bool IsActualGesture(string gestureId) {
			foreach (string str in IGNORED_GESTURE_IDS) {
				if (gestureId == str) {
					return false;
				}
			}

			return true;
		}

	}
}

