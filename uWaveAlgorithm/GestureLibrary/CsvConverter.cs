﻿using System;
using System.Collections.Generic;
using System.Text;

namespace uWaveAlgorithm.GestureLibrary
{
	internal class CsvConverter
	{
		internal string addTitle { get; set; }
		// string for the first row of the export

		internal List<List<object>> rows = new List<List<object>> ();

		List<object> currentRow {
			get {
				return rows [rows.Count - 1];
			}
		}


		string MakeValueCsvFriendly (object value)
		{
			if (value == null)
				return "";
			//       if (value is Nullable && ((INullable)value).IsNull) return "";
			if (value is DateTime) {
				if (((DateTime)value).TimeOfDay.TotalSeconds == 0)
					return ((DateTime)value).ToString ("yyyy-MM-dd");
				return ((DateTime)value).ToString ("yyyy-MM-dd HH:mm:ss");
			}
			string output = value.ToString ();
			if (output.Contains (",")) {
				output = output.Replace (",", ".");
			}
			if (output.Contains (",") || output.Contains ("\""))
				output = '"' + output.Replace ("\"", "\"\"") + '"';
			return output;

		}

		internal string Export ()
		{
			StringBuilder sb = new StringBuilder ();

			// if there is a title
			if (!string.IsNullOrEmpty (addTitle)) {
				// escape chars that would otherwise break the row / export
				char[] csvTokens = new[] { '\"', ',', '\n', '\r' };

				if (addTitle.IndexOfAny (csvTokens) >= 0) {
					addTitle = "\"" + addTitle.Replace ("\"", "\"\"") + "\"";
				}

				if (addTitle.Contains ("|")) {
					addTitle = addTitle.Replace ('|', '/');
				}

				sb.Append (addTitle).Append (",");
				sb.AppendLine ();
			}

			// The rows
			foreach (List<object> row in rows) {
				foreach (object item in row) {
					sb.Append (MakeValueCsvFriendly (item)).Append (",");
				}
				sb.AppendLine ();
			}
				
			return sb.ToString ();
		}

		internal byte[] ExportToBytes ()
		{
			return Encoding.UTF8.GetBytes (Export ());
		}

		internal string CleanTitle(string title) {
			return title.TrimEnd(new char[] { ',', '\n', '\r' });
		}

		internal IList<int[]> ImportDataRow(string row) {
			string[] data = row.Split (',');
			List<int[]> result = new List<int[]> ();
			foreach (string vector in data) {
				if (!string.IsNullOrWhiteSpace (vector)) {
					result.Add (ParseData (vector));
				}
			}

			return result;

		}

		private int[] ParseData(string data) {
			if (string.IsNullOrEmpty (data)) {
				throw new InvalidOperationException ();
			}

			string[] values = data.Split ('|');
			if (values.Length != 3) {
				throw new InvalidOperationException ();
			}

			int[] result = new int[3];
			for (int i = 0; i < 3; i++) {
				bool passed = int.TryParse (values [i], out result [i]);
				if (!passed) {
					throw new InvalidOperationException ();
				}
			}

			return result;
		}
	}

}
