﻿using System;
using System.Collections.Generic;

namespace uWaveAlgorithm.GestureLibrary
{
	internal class GestureTemplate
	{
		internal IList<int[]> Template { get; set; }
		internal DateTime TimeStamp { get; private set; }
		object LOCK = new object();

		double[,] DynamicDistanceMatrix;
		IList<int[]> InputVector;

		internal GestureTemplate ()
		{
			TimeStamp = DateTime.UtcNow;
		}

		internal double DTWDistance(IList<int[]> input) {
			lock (LOCK) {
				InputVector = input;
				DynamicDistanceMatrix = new double[Template.Count, InputVector.Count];

				if (Template.Count == 0) {
					throw new InvalidOperationException ("Template has no elements");
				}

				for (int i = 0; i < Template.Count; i++) {
					for (int j = 0; j < InputVector.Count; j++) {
						DynamicDistanceMatrix [i, j] = DTWDistance (i, j);
					}
				}
				return DynamicDistanceMatrix [Template.Count - 1, InputVector.Count - 1];

			}
		}

		private double DTWDistance(int templateIndex, int inputIndex) {
			if (Template.Count - 1 < templateIndex || InputVector.Count - 1 < inputIndex) {
				throw new InvalidOperationException ();
			}

			double d0 = EuclideanDistance (Template [templateIndex], InputVector [inputIndex]);

			double d1 = (templateIndex > 0) ? DynamicDistanceMatrix [templateIndex - 1, inputIndex] : double.MaxValue;
			double d2 = (inputIndex > 0) ? DynamicDistanceMatrix [templateIndex, inputIndex - 1] : double.MaxValue;
			double d3 = (inputIndex > 0 && templateIndex > 0) ? DynamicDistanceMatrix [templateIndex - 1, inputIndex - 1] : double.MaxValue;

			double dm = Math.Min (Math.Min (d1, d2), d3);

			if (inputIndex > 0 || templateIndex > 0) {
				d0 += dm;
			}

			return d0;
		}

		private double EuclideanDistance(int[] source, int[] target) {
			if (source.Length != target.Length) {
				throw new InvalidOperationException ();
			}

			int sum = 0;
			for (int i = 0; i < source.Length; i++) {
				int dif = source [i] - target [i];
				sum += dif * dif;
			}

			return Math.Sqrt (sum);
		}
	}
}

