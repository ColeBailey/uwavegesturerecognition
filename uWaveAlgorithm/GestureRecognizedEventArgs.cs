﻿using System;
using System.Collections.Generic;

namespace uWaveAlgorithm
{
	public class GestureRecognizedEventArgs : EventArgs
	{
		public string GestureId { get; set; }

		public double Cost { get; set; }

		public IList<int[]> Dataset { get; set; }

		public bool IsActualGesture { get; set; }

		public GestureRecognizedEventArgs ()
		{
		}
	}
}

