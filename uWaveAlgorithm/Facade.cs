﻿using System;
using uWaveAlgorithm.DataInterpretation;
using uWaveAlgorithm.GestureLibrary;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace uWaveAlgorithm
{
	public class Facade
	{
		private DataManager _dataManager;
		private readonly GestureManager _gestureManager;

		public string NextTrainingDataId { get; set; }
		public double MaximumCostAllowed { get; set; }
		public EventHandler<GestureRecognizedEventArgs> GestureRecognized;

		public Facade (double variance = 1.0)
		{
			MaximumCostAllowed = double.PositiveInfinity;
			_dataManager = new DataManager (variance);
			_gestureManager = new GestureManager ();
			_dataManager.DTWValueUpdate += ((sender, e) => {
				double cost;
				var recognized = _gestureManager.RecognizeGesture(e, out cost);
				if (!double.IsInfinity(MaximumCostAllowed) && cost > MaximumCostAllowed) {
					return;
				}

				if (!string.IsNullOrEmpty(recognized) && GestureRecognized != null) {
					GestureRecognized(this, new GestureRecognizedEventArgs { GestureId = recognized, Cost = cost, 
						Dataset = e, IsActualGesture = _gestureManager.IsActualGesture(recognized) });
				}

				if (!string.IsNullOrEmpty(NextTrainingDataId)) {
					_gestureManager.TrainNewGesture(NextTrainingDataId, e);
					NextTrainingDataId = string.Empty;
				}
			});
		}

		public void PositiveUpdate(GestureRecognizedEventArgs e) {
			_gestureManager.RemoveDataset (e.GestureId, e.Dataset);
			_gestureManager.RetrainGesture (e.GestureId, e.Dataset);
		}

		public void NegativeUpdate(GestureRecognizedEventArgs e, string actualGesture) {
			_gestureManager.RemoveDataset (e.GestureId, e.Dataset);
			_gestureManager.RetrainGesture (actualGesture, e.Dataset);
		}

		public void AccelerometerInput(double x, double y, double z) {
			_dataManager.FeedAccelerometerVector (x, y, z);
		}

		public string ExportGestureLibrary() {
			string result = string.Empty;
			foreach (string key in _gestureManager.Library.Keys) {
				GestureCollection collection = _gestureManager.Library [key];
				CsvConverter exporter = new CsvConverter ();
				exporter.addTitle = key;
				foreach (GestureTemplate template in collection.Templates) {
					List<object> input = new List<object> ();
					foreach (int[] values in template.Template) {
						input.Add (values [0] + "|" + values [1] + "|" + values [2]);
					}
					exporter.rows.Add (input);
				}
				result += exporter.Export ();
			}

			return result;
		}

		public void ImportGestureLibrary(string input) {
			if (string.IsNullOrEmpty (input)) {
				throw new InvalidOperationException ();
			}

			string[] lines = Regex.Split (input, "\r\n|\r|\n");

			if (lines.Length == 0) {
				throw new InvalidOperationException ();
			}

			string currentKey = string.Empty;
			CsvConverter importer = new CsvConverter ();

			for (int i = 0; i < lines.Length; i++) {
				string line = lines [i];
				if (string.IsNullOrWhiteSpace (line)) {
					continue;
				}

				if (!line.Contains ("|")) {		// title line
					currentKey = importer.CleanTitle (line);
				} else {
					if (!string.IsNullOrWhiteSpace(currentKey)) {
						_gestureManager.TrainNewGesture (currentKey, importer.ImportDataRow (line));
					}
				}
			}
		}
	}
}

