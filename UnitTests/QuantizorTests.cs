﻿using System;
using NUnit.Framework;
using uWaveAlgorithm;
using uWaveAlgorithm.DataInterpretation;

namespace UnitTests
{
	[TestFixture]
	public class QuantizorTests
	{
		
		public QuantizorTests ()
		{
		}

		[Test]
		public void TestNegative16Quantized() {
			var q = new Quantizor ();
			q.AddValue (-2 * 9.8 - 0.5);
			Assert.AreEqual (-16, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative15Quantized() {
			var q = new Quantizor ();
			q.AddValue (-2 * 9.8 + 0.5);
			Assert.AreEqual (-15, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative14Quantized() {
			var q = new Quantizor ();
			q.AddValue (-2 * 9.8 + 0.5 + 9.8 / 5);
			Assert.AreEqual (-14, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative13Quantized() {
			var q = new Quantizor ();
			q.AddValue (-2 * 9.8 + 0.5 + 2 * (9.8 / 5));
			Assert.AreEqual (-13, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative12Quantized() {
			var q = new Quantizor ();
			q.AddValue (-2 * 9.8 + 0.5 + 3 * (9.8 / 5));
			Assert.AreEqual (-12, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative11Quantized() {
			var q = new Quantizor ();
			q.AddValue (-2 * 9.8 + 0.5 + 4 * (9.8 / 5));
			Assert.AreEqual (-11, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative10Quantized() {
			var q = new Quantizor ();
			q.AddValue (-1 * 9.8 + 0.5);
			Assert.AreEqual (-10, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative9Quantized() {
			var q = new Quantizor ();
			q.AddValue (-1 * 9.8 + 0.5 + 1 * (9.8 / 10));
			Assert.AreEqual (-9, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative8Quantized() {
			var q = new Quantizor ();
			q.AddValue (-1 * 9.8 + 0.5 + 2 * (9.8 / 10));
			Assert.AreEqual (-8, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative7Quantized() {
			var q = new Quantizor ();
			q.AddValue (-1 * 9.8 + 0.5 + 3 * (9.8 / 10));
			Assert.AreEqual (-7, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative6Quantized() {
			var q = new Quantizor ();
			q.AddValue (-1 * 9.8 + 0.5 + 4 * (9.8 / 10));
			Assert.AreEqual (-6, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative5Quantized() {
			var q = new Quantizor ();
			q.AddValue (-1 * 9.8 + 0.5 + 5 * (9.8 / 10));
			Assert.AreEqual (-5, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative4Quantized() {
			var q = new Quantizor ();
			q.AddValue (-1 * 9.8 + 0.5 + 6 * (9.8 / 10));
			Assert.AreEqual (-4, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative3Quantized() {
			var q = new Quantizor ();
			q.AddValue (-1 * 9.8 + 0.5 + 7 * (9.8 / 10));
			Assert.AreEqual (-3, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative2Quantized() {
			var q = new Quantizor ();
			q.AddValue (-1 * 9.8 + 0.5 + 8 * (9.8 / 10));
			Assert.AreEqual (-2, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestNegative1Quantized() {
			var q = new Quantizor ();
			q.AddValue (-1 * 9.8 + 0.5 + 9 * (9.8 / 10));
			Assert.AreEqual (-1, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void Test0Quantized() {
			var q = new Quantizor ();
			q.AddValue (0.0);
			Assert.AreEqual (0, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive16Quantized() {
			var q = new Quantizor ();
			q.AddValue (2 * 9.8 + 0.5);
			Assert.AreEqual (16, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive15Quantized() {
			var q = new Quantizor ();
			q.AddValue (2 * 9.8 - 0.5);
			Assert.AreEqual (15, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive14Quantized() {
			var q = new Quantizor ();
			q.AddValue (2 * 9.8 - 0.5 - 9.8 / 5);
			Assert.AreEqual (14, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive13Quantized() {
			var q = new Quantizor ();
			q.AddValue (2 * 9.8 - 0.5 - 2 * (9.8 / 5));
			Assert.AreEqual (13, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive12Quantized() {
			var q = new Quantizor ();
			q.AddValue (2 * 9.8 - 0.5 - 3 * (9.8 / 5));
			Assert.AreEqual (12, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive11Quantized() {
			var q = new Quantizor ();
			q.AddValue (2 * 9.8 - 0.5 - 4 * (9.8 / 5));
			Assert.AreEqual (11, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive10Quantized() {
			var q = new Quantizor ();
			q.AddValue (1 * 9.8 - 0.5);
			Assert.AreEqual (10, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive9Quantized() {
			var q = new Quantizor ();
			q.AddValue (1 * 9.8 - 0.5 - 1 * (9.8 / 10));
			Assert.AreEqual (9, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive8Quantized() {
			var q = new Quantizor ();
			q.AddValue (1 * 9.8 - 0.5 - 2 * (9.8 / 10));
			Assert.AreEqual (8, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive7Quantized() {
			var q = new Quantizor ();
			q.AddValue (1 * 9.8 - 0.5 - 3 * (9.8 / 10));
			Assert.AreEqual (7, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive6Quantized() {
			var q = new Quantizor ();
			q.AddValue (1 * 9.8 - 0.5 - 4 * (9.8 / 10));
			Assert.AreEqual (6, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive5Quantized() {
			var q = new Quantizor ();
			q.AddValue (1 * 9.8 - 0.5 - 5 * (9.8 / 10));
			Assert.AreEqual (5, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive4Quantized() {
			var q = new Quantizor ();
			q.AddValue (1 * 9.8 - 0.5 - 6 * (9.8 / 10));
			Assert.AreEqual (4, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive3Quantized() {
			var q = new Quantizor ();
			q.AddValue (1 * 9.8 - 0.5 - 7 * (9.8 / 10));
			Assert.AreEqual (3, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive2Quantized() {
			var q = new Quantizor ();
			q.AddValue (1 * 9.8 - 0.5 - 8 * (9.8 / 10));
			Assert.AreEqual (2, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestPositive1Quantized() {
			var q = new Quantizor ();
			q.AddValue (1 * 9.8 - 0.5 - 9 * (9.8 / 10));
			Assert.AreEqual (1, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestAveraged0() {
			var q = new Quantizor ();
			q.AddValue (54.34);
			q.AddValue (-32.1245);
			q.AddValue (32.1245);
			q.AddValue (-54.34);
			Assert.AreEqual (0, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestAveraged10() {
			var q = new Quantizor ();
			q.AddValue (9.8);
			q.AddValue (9.91);
			q.AddValue (9.82);
			q.AddValue (9.3);
			q.AddValue (9.458);
			q.AddValue (9.7);
			Assert.AreEqual (10, q.RetrieveQuantizedValue ());
		}

		[Test]
		public void TestAveragedNegative10() {
			var q = new Quantizor ();
			q.AddValue (-9.8);
			q.AddValue (-9.91);
			q.AddValue (-9.82);
			q.AddValue (-9.3);
			q.AddValue (-9.458);
			q.AddValue (-9.7);
			Assert.AreEqual (-10, q.RetrieveQuantizedValue ());
		}
	}
}

