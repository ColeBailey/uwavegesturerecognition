﻿using System;
using NUnit.Framework;
using uWaveAlgorithm.DataInterpretation;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace UnitTests
{
	[TestFixture]
	public class DataManagerTests
	{
		public DataManagerTests ()
		{
		}

		[Test]
		public async void TestBasicDTW() {
			var dm = new DataManager (-1);

			double feed = 0.0;
			for (int i = 0; i < 15; i++) {
				if (i == 1) {
					feed += 0.5;
				} else if (1 < i && i < 11) {
					feed += 0.98;
				} else if (i >= 11) {
					feed += 2 * 0.98;
				}
				dm.FeedAccelerometerVector (feed, feed, feed);
				await Task.Delay (50);
			}

			
			
			var result = new List<int[]>(dm.CurrentDTW);

			for (int i = 0; i < result.Count; i++) {
				var x = result [i] [0];
				Assert.AreEqual (i, x);
				Assert.AreEqual (i, result [i] [1]);
				Assert.AreEqual (i, result [i] [2]);
			}

		}

		[Test]
		public async void TestConstantValues() {
			var dm = new DataManager ();

			dm.DTWValueUpdate += (sender, e) => {
				Assert.AreEqual(30, e.Count);
				for (int i = 0; i < e.Count; i++) {
					Assert.AreEqual (3, e[i].Length);
					Assert.AreEqual (10, e[i][0]);
					Assert.AreEqual (10, e[i][1]);
					Assert.AreEqual (10, e[i][2]);
				}
			};

			const double feed = 9.7;
			for (int i = 0; i < 30; i++) {
				dm.FeedAccelerometerVector (feed, feed, feed);
				await Task.Delay (50);
			}

			for (int i = 0; i < 8; i++) {
				dm.FeedAccelerometerVector (9.8, 0, 0);
				await Task.Delay (50);
			}


		}
	}
}

